# Doc Generation for ULib

How to generate the documentation website from the .d.ts files.

## Prerequisites

### TypeDoc

The first thing you need, it TypeDoc. Follow the instructions to install it here:
https://github.com/TypeStrong/typedoc

## Generating the docs

Just run the following commandline:

```
typedoc --out out/ src/ --mode modules --module commonjs --excludePrivate --includeDeclarations
```

 - out/ is the path to your output folder.
 - src/ is the path to the folder containing all the .d.ts files.
 
## ToDo

 - Shows interfaces on the main module page (would probably prefer to hide these, but maybe not a big deal).
 - Shows line number where code is defined e.g. "Defined in u-actions.d.ts:29". - can add display:none to .tsd-sources class in main.css to hide it.


You can add code examples to comments e.g. <pre><code>var foo = 'hello';</code></pre>
You can add docs to each value of an enum but putting a comment line above it e.g. /** Page component thingy. */


## Ref
 - https://github.com/TypeStrong/typedoc
 - http://typedoc.org/guides/doccomments/
 
