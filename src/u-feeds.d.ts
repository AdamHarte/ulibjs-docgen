/** 
 * @module umajin
 */

/** */
declare module "u-feeds" {
	export class Feed {
		/** 
		 * Create custom Feed that can show up inside Umajin App Creator.
		 * @param name Display name of the Feed that will appear in Umajin App Creator.
		 * @param callback Reference to the function that will be called when the Feed is requested. Must return feed data as a json string, array, or object.
		 */
		constructor(name:string, callback:Function);
		
		/** Call register once the Feed is ready, to add it into Umajin App Creator. */
		register():Feed;
	}

	export class FeedMapping {
		constructor();
		add(jsonKey:string, componentName:string);
	}
}

