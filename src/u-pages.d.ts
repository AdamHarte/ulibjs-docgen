/** 
 * @module umajin
 */

/** */
declare module "u-pages" {
	import {Page} from "u-components";
	
	export enum PageTransition {
		NONE,
		SLIDE,
		FADE,
		FLY_IN,
		COVER_FLOW,
		ROTATE,
		WIPE
	}
	
	/** Reference to the current page. */
	export var current: Page;
	
	/** 
	 * Get a page by name.
	 * @param pageName The name of page in Umajin App Creator that you want to get.
	 */
	export function get(pageName:string):Page;
	
	/** Get a list of all pages in the app. */
	//all():Array<Page>;
}

