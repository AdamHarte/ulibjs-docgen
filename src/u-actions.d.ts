/** 
 * @module umajin
 */

/** */
declare module "u-actions" {
	import {PageTransition} from "u-pages";
	import {ComponentType} from "u-components";
	
	/** Constants for all Action Parameter Types. */
	export enum ParamType {
		BOOLEAN,
		STRING,
		HARD_CODED,
		INT,
		REAL,
		FILENAME,
		
		PAGE,
		FOREGROUND,
		BUTTON,
		IMAGE,
		TEXT,
		TEXT_ENTRY,
		HTML_ARTICLE,
		VIDEO,
		GALLERY,
		GLASS_SHELF,
		ANIMATED_FEED,
		FEED_ITEM_VIEW,
		FEED_LIST,
		CAROUSEL,
		ANIMATION,
		IMAGE_ZOOMER,
		WEB_VIEW,
		SCROLL_PANEL,
		PRODUCT_LIST,
		GAME_KIT,
		RECTANGLE,
		CIRCLE,
		MAP_VIEW,
		NINE_SLICE
	}
	
	export class Action {
		/** 
		 * Create an Action that can show up inside Umajin App Creator. 
		 * @param name Display name of the Action that will appear in Umajin App Creator.
		 * @param callback Reference to the function that will be called when the Action is fired.
		 */
		constructor(name:string, callback:Function);
		
		/** 
		 * Add a typed parameter to the action. The value is sent to the callback.
		 * @param name Name of the parameter that will show in Umajin App Creator.
		 * @param type Parameter type can be any of the parameter types from "actions.ParamType". If none is specified, then no filter is applied.
		 */
		addParameter(name:string, type?:ParamType):Action;
		
		/**
		 * Add a String parameter to the action. The value is sent to the callback.
		 * @param name Name of the parameter that will show in Umajin App Creator.
		 * @param defaultValue The initial value the parameter is set to.
		 */
		addStringParameter(name:string, defaultValue?:string):Action;
		
		/**
		 * Add a Boolean parameter to the action. The value is sent to the callback.
		 * @param name Name of the parameter that will show in Umajin App Creator.
		 * @param defaultValue The initial value the parameter is set to.
		 */
		addBooleanParameter(name:string, defaultValue?:boolean):Action;
		
		/**
		 * Add a Hard Coded parameter to the action. The value is sent to the callback. They use can no change this value.
		 * @param name Name of the parameter that will show in Umajin App Creator.
		 * @param defaultValue The value the parameter is set to.
		 */
		addHardCodedParameter(name:string, value?:string):Action;
		
		/**
		 * Add a Int parameter to the action, that shows a slider. The value is sent to the callback.
		 * @param name Name of the parameter that will show in Umajin App Creator.
		 * @param defaultValue The initial value the parameter is set to.
		 * @param min The minimum value that the use can set.
		 * @param max The maximum value that the use can set.
		 */
		addIntParameter(name:string, defaultValue:number, min:number, max:number):Action;
		
		/**
		 * Add a Real parameter to the action, that shows a slider. The value is sent to the callback.
		 * @param name Name of the parameter that will show in Umajin App Creator.
		 * @param defaultValue The initial value the parameter is set to.
		 * @param min The minimum value that the use can set.
		 * @param max The maximum value that the use can set.
		 */
		addRealParameter(name:string, defaultValue:number, min:number, max:number):Action;
		
		/** Add a custom icon to the action. */
		addIcon(filename:string):Action; //TODO: Get custom action icons working.
		
		/** Add a custom description to the action. */
		addDescription(description:string);
		
		/** Call register once the Action is ready, to add it into Umajin App Creator. */
		register():Action;
	}
	
	interface NavigateActions {
		/** 
		 * Navigate to a page with page ID.
		 * @param pageId The id of the page that will be navigated to.
		 */
		showPage(pageId:string):void;
		
		/** 
		 * Navigate to a page with Page name.
		 * @param pageName The name of the page to navigate to.
		 * @param transitionType How the page transition. Use "Umajin.transition". Defaults to NONE.
		 * @param direction Defaults to "auto", but can force transition to run "forward" or "backward".
		 */
		showPageByName(pageName:string, animationType:PageTransition, direction:string):void;
		
		/** 
		 * Navigate to a page with Page name without adding an entry to the history.
		 * @param pageName The name of the page to navigate to.
		 * @param transitionType How the page transition. Use "Umajin.transition". Defaults to NONE.
		 * @param direction Defaults to "auto", but can force transition to run "forward" or "backward".
		 */
		showPageByNameNoHistory(pageName:string, animationType:PageTransition, direction:string):void;
		
		/** Navigate back to the previous page. */
		back():void;
	}

	interface BluetoothActions {
		frequency: number;
		enable():void;
		connect(deviceName:string):void;
		
		/** Read from a characteristic of a service. */
		read(serviceId:string, characteristicId:string, callback:Function):void;
		
		/** Write to a characteristic of a service. */
		write(serviceId:string, characteristicId:string, value:string):void;
		clearState(componentId:string):void;
	}

	interface SocialActions {
		followOnTwitter(twitterName:string):void;
		shareOnTwitter(message:string, url:string):void;
		followOnFacebook(userId:string):void;
		shareOnFacebook(url:string):void;
		followOnGooglePlus(userId:string):void;
		shareOnGooglePlus(url:string):void;
	}

	interface SoundActions {
		play(audioFile:string, loop:boolean, volume:number):void;
		stop(audioFile:string):void;
		stopAll():void;
	}

	interface NativeActions {
		/** Show all hidden native overlays. */
		showOverlays():void;
		
		/** Hide all visible native overlays. */
		hideOverlays():void;
	}
	
	/** Navigate between pages. */
	export var navigate: NavigateActions;
	
	/** Access bluetooth devices. */
	export var bluetooth: BluetoothActions;
	
	/** Access features from popular social networks. */
	export var social: SocialActions;
	
	/** Control audio within the app. */
	export var sound: SoundActions;
	
	/** Access native overlays. */
	export var native: NativeActions;
	
	/** 
	 * Display a popup with a message.
	 * @param message The message to display in the popup.
	 */
	export function showPopup(message:string):void;
	
	/**
	 * Show a combo box.
	 * @param callback The name of the callback function.
	 * @param options An Array of item names as Strings.
	 */
	export function showCombo(callback:string, options:string[]):void; // options is actually a pipe seperated string, so need to join them in javascript lib.
	
	/** 
	 * Show a web page using the default web browser app.
	 * @param url The url to show.
	 */
	export function showWebPage(url:string):void;
	
	/** 
	 * Show a location on the map using the default map app.
	 * @param location Address of the location to show.
	 */
	export function openMap(location:string):void;
	
	/** Make a phone call using the default dialer app. */
	export function dialNumber(phoneNumber:string):void;
	
	/** Send an SMS message using the default SMS app. */
	export function sendSMS(phoneNumber:string):void;
	
	/** Send an email */
	export function sendEmail(to:string, cc:string, bcc:string, subject:string, body:string, attachment:string, mimeType:string):void;
	
	/** Capture a photo from the camera, and call back the specified function with the resulting filename, or blank if cancelled */
	export function cameraCapture(callback:string):void;
		
	//TODO: Make delay calls work with function references rather than strings.
	//delayCall(callback:Function, delay:number):void;
	//cancelDelayedCalls(callback:Function):void;
	//cancelAllDelayedCalls():void;
	
	
	
	// add_to_cart
	// remove_from_cart
	// cart_delivery_address
	// set_cart_option_delivered
	// cart_update
	// cart_clear
	// cart_checkout
	// cart_verify
	
	// set_loyalty_adv_store
	// set_loyalty_store
	
	// show_product_customiser
	
	// set_form_data_value
	
	// log_emotion
	
	// start_fingerprint_scanner
	// open_fingerprint_scanner_dialog
	
	// notify_later
	// notify_now
	// notify_page_change
	
	//create(callback:string, name:string, icon:string, params:string):void;
}

