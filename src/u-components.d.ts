/** 
 * This is a comment at the top.
 * @module umajin
 * @preferred
 */

/** 
 * Module for all the stuff about components.
 */
declare module "u-components" {
	import {PageTransition} from "u-pages";
	import {TweenType} from "u-tween";
	
	export enum ComponentType {
		PAGE,
		FOREGROUND,
		BUTTON,
		IMAGE,
		TEXT,
		TEXT_ENTRY,
		HTML_ARTICLE,
		VIDEO,
		GALLERY,
		GLASS_SHELF,
		ANIMATED_FEED,
		FEED_ITEM_VIEW,
		FEED_LIST,
		CAROUSEL,
		ANIMATION,
		IMAGE_ZOOMER,
		WEB_VIEW,
		SCROLL_PANEL,
		PRODUCT_LIST,
		GAME_KIT,
		RECTANGLE,
		CIRCLE,
		MAP_VIEW,
		NINE_SLICE
	}
	
	export enum AlignType {
		LEFT,
		CENTER,
		RIGHT,
		JUSTIFIED
	}
	
	export enum ResizeType {
		MILLIMETERS,
		SCALE_WITH_SCREEN
	}
	
	export enum TextScaling {
		FIXED,
		SCALE_WITH_SCREEN
	}
	
	export enum ComponentEvent {
		PRESS,
		DOWN,
		MOVE,
		UP,
		SWIPE_UP,
		SWIPE_DOWN,
		SWIPE_LEFT,
		SWIPE_RIGHT,
		DROP
	}
	
	interface UmajinCast {
		to(component:any):Component;
		toPage(component:Component):Page;
		toForeground(component:Component):Foreground;
		toButton(component:Component):ButtonComponent;
		toImage(component:Component):ImageComponent;
		toText(component:Component):TextComponent;
		toTextEntry(component:Component):TextEntryComponent;
		toHTMLArticle(component:Component):HTMLArticleComponent;
		toVideo(component:Component):VideoComponent;
		toGallery(component:Component):GalleryComponent;
		toGlassShelf(component:Component):GlassShelfComponent;
		toAnimatedFeed(component:Component):AnimatedFeedComponent;
		toFeedItemView(component:Component):FeedItemViewComponent;
		toFeedList(component:Component):FeedListComponent;
		toCarousel(component:Component):CarouselComponent;
		toAnimation(component:Component):AnimationComponent;
		toImageZoomer(component:Component):ImageZoomerComponent;
		toWebView(component:Component):WebViewComponent;
		toScrollPanel(component:Component):ScrollPanelComponent;
		toProductList(component:Component):ProductListComponent;
		toGameKit(component:Component):GameKitComponent;
		toRectangle(component:Component):RectangleComponent;
		toCircle(component:Component):CircleComponent;
		toMapView(component:Component):MapViewComponent;
		toNineSlice(component:Component):NineSliceComponent;
	}
	
	interface ComponentList {
		//all():Array<Component>; //TODO: This might not work because we would need the names and the types.
		get(componentName:string, componentType:ComponentType):Component;
		getButton(componentName:string):ButtonComponent;
		getImage(componentName:string):ImageComponent;
		getText(componentName:string):TextComponent;
		getTextEntry(componentName:string):TextEntryComponent;
		getHTMLArticle(componentName:string):HTMLArticleComponent;
		getVideo(componentName:string):VideoComponent;
		getGallery(componentName:string):GalleryComponent;
		getGlassShelf(componentName:string):GlassShelfComponent;
		getAnimatedFeed(componentName:string):AnimatedFeedComponent;
		getFeedItemView(componentName:string):FeedItemViewComponent;
		getFeedList(componentName:string):FeedListComponent;
		getCarousel(componentName:string):CarouselComponent;
		getAnimation(componentName:string):AnimationComponent;
		getImageZoomer(componentName:string):ImageZoomerComponent;
		getWebView(componentName:string):WebViewComponent;
		getScrollPanel(componentName:string):ScrollPanelComponent;
		getProductList(componentName:string):ProductListComponent;
		getGameKit(componentName:string):GameKitComponent;
		getRectangle(componentName:string):RectangleComponent;
		getCircle(componentName:string):CircleComponent;
		getMapView(componentName:string):MapViewComponent;
		getNineSlice(componentName:string):NineSliceComponent;
	}
	
	export class Component {
		get(propertyName:string):any;
		set(propertyName:string, value:any):void;
		on(eventType:string, callback:Function):void;
		emit(eventType:string, data:Object):void;
		
		/**
		 * Tween a component property over time.
		 * @param seconds The number of seconds to tween for.
		 * @param params An objects that contains key/value pairs of the properties and values to tween.
		 * @param type (Optional) Tween type can be any type from "Tween.type"
		 */
		tween(seconds:number, params:Object, type?:TweenType):void;
		
		uniqueId: string;
		name: string;
		
		/** Access this Component's sub-components. */
		components: ComponentList;
		
		// hitMode: number;
		x: number;
		y: number;
		width: number;
		height: number;
		visible: boolean;
		// angle: number;
		// scale: number;
		// scaleWidth: number;
		// scaleHeight: number;
		// originX: number;
		// originY: number;
		alpha: number; // No getter
		
		// Should only be getters, no setters!
		//lock_left
		//lock_left_size
		//lock_width
		//lock_width_size
		//lock_right
		//lock_right_size
		//lock_top
		//lock_top_size
		//lock_height
		//lock_height_size
		//lock_bottom
		//lock_bottom_size
		//instance_display_name
	}
	
	export class ButtonComponent extends Component {
		enabled: boolean;
		
		text: string;
		defaultFilename: string;
		downFilename: string;
		disabledFilename: string;
		
		sliceTop: number;
		sliceBottom: number;
		sliceLeft: number;
		sliceRight: number;
		borderScale: number;
		
		hideText: boolean;
		keepAspectRatio: boolean;
		iconFilename: string;
		iconScale: number;
		iconX: number;
		iconY: number;
		textX: number;
		textY: number;
		
		fontColor: string;
		fontSize: number;
		font: string;
		textScaling: TextScaling;
		scaleBorderWithScreen: boolean;
		tracking: number;
	}

	export class ImageComponent extends Component {
		filename: string;
		/** { 0:Fill, 1:Fit, 2:Stretch } */
		cropMode: number;
		nativeWidth: number;
		nativeHeight: number;
		alignX: number;
		alignY: number;
	}

	export class TextComponent extends Component {
		text: string;
		fontColor: string;
		//multiline: boolean;
		
		nativeHeight: number;
		fontSize: number;
		font: string;
		centerVertical: boolean;
		align: AlignType;
		resizeType: ResizeType;
	}

	export class TextEntryComponent extends Component {
		text: string;
	}

	export class HTMLArticleComponent extends Component {
		html: string;
	}

	export class VideoComponent extends Component {
		filename: string;
		play(filename:string):void;
		stop():void;
	}

	export class GalleryComponent extends Component {
		filename: string;
		goToFirst(animate:boolean): void;
		goToLast(animate:boolean): void;
		goToNext(animate:boolean): void;
		goToPrevious(animate:boolean): void;
		zoomFull(animate:boolean): void;
		zoomFit(animate:boolean): void;
		zoomIn(animate:boolean): void;
		zoomOut(animate:boolean): void;
		zoomTo(zoomLevel:number): void;
		pan(zoomLevel:number, horizontal:number, vertical:number): void;
		setFolder(folderPath:string): void;
	}

	export class CarouselComponent extends Component {
		itemsCount: number; //TODO: Getter only.
		speed: number;
	}

	export class GlassShelfComponent extends Component {
		itemsCount: number; //TODO: Getter only.
		addItem(filename:string):void;
		removeItem(index:number):void;
	}

	export class DataBoundItemsBase extends Component {
		dataId: string;
		scrollOffset: number;
		scrollAnimateY: number;
		scrollMax: number;
		dataUrl: string;
		json: string;
		mapping: string;
		template: string;
	}

	export class AnimatedFeedComponent extends DataBoundItemsBase {
		forceFeedUpdate():void;
	}

	export class FeedItemViewComponent extends DataBoundItemsBase {
		url: string;
		setFeedItem(targetIndex:number, mode:string, customIndex:number):void;
		forceFeedUpdate():void;
	}

	export class FeedListComponent extends DataBoundItemsBase {
		url: string;
		applyFilter(filter:string, columns:string, textField?:TextEntryComponent):void;
		clearFilter():void;
		forceFeedUpdate():void;
		reset():void;
		setCustomFeed(jsonData:Array<Object>);
	}

	export class AnimationComponent extends Component {
		play(animation:string, loop:boolean):void;
		stop():void;
	}

	export class ImageZoomerComponent extends Component {
		filename: string;
	}

	export class WebViewComponent extends Component {
		url: string;
	}

	export class ScrollPanelComponent extends Component {
		reset():void;
	}

	export class ProductListComponent extends Component {
		reset():void;
	}

	export class GameKitComponent extends Component {
		sprites: Array<GameKitSprite>;
		addSprite(sprite:GameKitSprite):void;
		getSprite(cellId:number, imageName:string):GameKitSprite;
		trigger(eventId:string, data:string):void;
	}

	export class RectangleComponent extends Component {
		fillColor: string;
	}

	export class CircleComponent extends Component {
		fillColor: string;
	}

	export class MapViewComponent extends Component {
		showLatLong(latitude:number, longitude:number, zoom_level:number):void;
		addMapMarkerLatLong(latitude:number, longitude:number, title:string, description:string, image:string, zoomToMarker:boolean, showInfo:boolean):void;				
		addMapRouteAddress(startAddress, endAddress, zoomToMarker, routeType, showInfo):void;
	}

	export class NineSliceComponent extends Component {
		filename: string;
		sliceTop: number;
		sliceBottom: number;
		sliceLeft: number;
		sliceRight: number;
		borderScale: number;
	}
	
	export class GameKitSprite {
		constructor(cellId:number, imageName:string, x:number, y:number, scale:number, angle:number, alpha:number);
		width: number;
		height: number;
		update(idX:number, x:number, y:number, scale:number, angle:number, alpha:number):void;
	}
	
	export class Page {
		/** The name of the page that shows in Umajin App Creator. */
		name: string;
		
		/** A list of all the components directly on the page. */
		components: ComponentList;
		
		width: number;
		height: number;
		
		/** 
		 * Navigate to this page.
		 * @param transitionType How the page transition. Use "Umajin.transition". Defaults to NONE.
		 * @param direction Defaults to "auto", but can force transition to run "forward" or "backward".
		 */
		show(transitionType?:PageTransition, direction?:string):void;
		
		/** 
		 * Navigate to this page, but without adding to navigation history.
		 * @param transitionType How the page transition. Use "Umajin.transition". Defaults to NONE.
		 * @param direction Defaults to "auto", but can force transition to run "forward" or "backward".
		 */
		showNoHistory(transitionType?:PageTransition, direction?:string):void;
		
		/** Reset all components on the page back to their defaults. */
		reset();
	}
	
	export class Foreground {
		/** The name of the foreground that shows in Umajin App Creator. */
		name: string;
		
		/** A list of all the components directly on the foreground. */
		components: ComponentList;
		
		/** Set this as the current foreground for the current page. */
		show():void;
		
		/** Hides this foreground for the current page. */
		hide():void;
	}
	
	export var cast: UmajinCast;
}

