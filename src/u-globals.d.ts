/** 
 * @module umajin
 */

/** */
declare module "u-globals" {
	/** Constants for all Global Event Types. */
	export enum EventType {
		RESIZE,
		ACTIVATE,
		DEACTIVATE,
		RESUME,
		SUSPEND,
		PUSH_NOTIFICATION,
		EDIT_MODE,
		DEVICE_MOTION
	}
	
	interface GlobalComponents {
		autoCompleteComboBox: GAutoCompleteComboBox;
		location: GLocation;
	}

	interface GlobalComponent {
	}

	interface GAutoCompleteComboBox extends GlobalComponent {
		/** If true, the first item is selected when return is typed. */
		autoSelectOnReturn: boolean;
		
		/** 
		 * Show the Combo Box popup with auto-complete.
		 * @param text Default text to display in the text entry field.
		 * @param heading Text to display as the heading of the combo box popup.
		 * @param options An array of strings to use as the initial auto-complete data.
		 * @param onSelectCallback Callback when an item is selected from the combo box.
		 * @param onTypedCallback (Optional) Callback whenever something is typed into the entry field.
		 * @param entryType (Optional) The type of OSK to use ({0:default, 1:phone number, 2:email, 3:url}).
		 */
		show(text:string, heading:string, options:Array<string>, onSelectCallback:Function, onTypedCallback?:Function, entryType?:number):void;
		
		/** Close the Combo Box popup. */
		hide():void;
		
		/** Update the list of auto-complete options to a new list. */
		update(options:Array<string>):void;
	}

	interface GLocation extends GlobalComponent {
		getCurrent(onLocationFoundCallback:Function, onError:Function):void;
	}
	
	/** Access to all global Umajin components. */
	export var components: GlobalComponents;
	
	/** Get a global Umajin property by name. */
	export function get(name:string):any;
	
	/** Set a global Umajin property by name. */
	export function set(name:string, value:any):void;
	
	export function on(event:EventType, callback:Function):void;
}

