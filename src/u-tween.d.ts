/** 
 * @module umajin
 */

/** */
declare module "u-tween" {
	import {Component} from "u-components";
	
	export enum TweenType {
		NONE,
		LINEAR,
		UNIFORM,
		QUAD_IN,
		QUAD_OUT,
		QUAD_IN_OUT,
		QUAD_OUT_IN,
		EASE_IN,
		EASE_OUT,
		EASE_BOTH,
		CUBIC_IN,
		CUBIC_OUT,
		CUBIC_IN_OUT,
		CUBIC_OUT_IN,
		QUART_IN,
		QUART_OUT,
		QUART_IN_OUT,
		QUART_OUT_IN,
		EASE_IN_STRONG,
		EASE_OUT_STRONG,
		EASE_BOTH_STRONG,
		QUINT_IN,
		QUINT_OUT,
		QUINT_IN_OUT,
		QUINT_OUT_IN,
		SINE_IN,
		SINE_OUT,
		SINE_IN_OUT,
		SINE_OUT_IN,
		EXPO_IN,
		EXPO_OUT,
		EXPO_IN_OUT,
		EXPO_OUT_IN,
		CIRC_IN,
		CIRC_OUT,
		CIRC_IN_OUT,
		CIRC_OUT_IN,
		ELASTIC_IN,
		ELASTIC_OUT,
		ELASTIC_IN_OUT,
		ELASTIC_OUT_IN,
		ELASTIC_BOTH,
		BACK_IN,
		BACK_OUT,
		BACK_IN_OUT,
		BACK_OUT_IN,
		BACK_BOTH,
		BOUNCE_IN,
		BOUNCE_OUT,
		BOUNCE_IN_OUT,
		BOUNCE_OUT_IN,
		BOUNCE_BOTH,
		SIMPLEX16,
		CURVE_IN,
		CURVE_OUT,
		CURVE_SINE,
		CURVE_COSINE
	}
	
	export function to(target:Component, seconds:number, params:Object, type?:TweenType):void;
}

