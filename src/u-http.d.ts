/** 
 * @module umajin
 */

/** */
declare module "u-http" {
	export class Request {
		/** 
		 * Create a new HTTP Request. 
		 * @param url The URL to be requested.
		 */
		constructor(url:string);
		
		/** Add a URL parameter. */
		addParameter(name:string, value:string):Request;
		
		/** Add a header to the request. */
		addHeader(name:string, value:string):Request;
		
		addData(string:string):Request;
		
		/** 
		 * Set the callback function for a successful request.
		 * @param callback Reference to the function that will be called when the request is successful.
		 * @param parameters An Array of parameters that will be send to the callback when it is called.
		 */
		onSuccess(callback:Function, parameters?:Array<any>):Request;
		
		/** 
		 * Set the callback function for a failed request.
		 * @param callback Reference to the function that will be called when the request fails.
		 * @param parameters An Array of parameters that will be send to the callback when it is called.
		 */
		onError(callback:Function, parameters?:Array<any>):Request;
		
		/** Perform an HTTP Get request. */
		get():Request;
		
		/** Perform an HTTP Post request. */
		post():Request;
		
		/** Perform an HTTP Put request. */
		put():Request;
	}
}

