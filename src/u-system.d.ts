/** 
 * @module umajin
 */

/** */
declare module "u-system" {
	export var upTime: number;
	//export var tzOffset: number;
	//export var utc: number;
	export var os: string;
	export var compiled: boolean;
	export var clipboard: string;
	export var latitude: number;
	export var longitude: number;
	export var countryCode: string;
	export var countryName: string;
	export var countryLatitude: number;
	export var countryLongitude: number;
	export var countryLocation: string;
	export var language: string;
	export var deviceId: string;
	export var projectToken: string;
	export var projectId: number;
	export var spinnerFilename: string; //TODO: Could spinner things be moved somewhere else?
	export var spinnerSpeed: number;
	export var httpSpinner: boolean;
	export var httpBlockerAlpha: number;
	export var apeId: string;
}

